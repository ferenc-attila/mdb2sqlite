import sys

print("Checking necessary python modules...")
try:
    from osgeo import ogr, osr, gdal, apt
except:
    sys.exit('ERROR: cannot find GDAL/OGR modules')

print("Checking necessary executables...")

try:
    subprocess.call(["ogr2ogr", "ogrinfo"])
except OSError as e:
    if e.errno == errno.ENOENT:
        # handle file not found error.
    else:
        # Something else went wrong while trying to run `ogr2ogr or ogrinfo`
        raise

print("Checking ready to use OGR mdb drivers...")
import ogr
cnt = ogr.GetDriverCount()
formatsList = []  # Empty List

for i in range(cnt):
    driver = ogr.GetDriver(i)
    driverName = driver.GetName()
    if not driverName in formatsList:
        formatsList.append(driverName)

# Sorting the messy list of ogr drivers
formatsList.sort()

mdbdrivers = [str('Pgeo'), str('MDB')]

lack=list(set(mdbdrivers)-set(formatsList))

try:
    if lack is None:
        print('All MDB drivers are ready to use.')
except lack.count(2) as e:
    sys.exit('ERROR: cannot find any OGR mdb drivers')
else:
    print('WARNING: the \''+str(lack[0])+'\' OGR mdb driver is not installed or configured correctly.')

# We have to check the configuration somehow
# There will be 2 if branches. If lack is None, check teh Pgeo and the MDB also. If lack has one element, we will have to check
# the other one. There will be 2 functions to make this. We will need a readydrivers list variable.

function (check_PGeo)
