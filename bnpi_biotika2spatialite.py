#!/usr/bin/env python
import sys, subprocess, os, re
import pandas as pd

DATABASE = str("Biotika_v11.mdb")
NEW_DATABASE = str(re.sub('mdb', 'sqlite', DATABASE, re.IGNORECASE))

# Get mdb version. Later it will be important for encoding characters.
mdb_ver = subprocess.Popen(["mdb-ver", DATABASE],
            stdout = subprocess.PIPE).communicate()[0]
mdb_ver = mdb_ver.decode()

# Get list of tables in geodatabase
tables = subprocess.Popen(["mdb-tables", "-1", DATABASE],
            stdout = subprocess.PIPE).communicate()[0]
tables = tables.splitlines()

# Remove geodatabase specific tables (it will be written into for loop or function).
tables = [table for table in tables if not re.match(b'^GDB_', table, re.IGNORECASE)]
tables = [table for table in tables if not re.match(b'~TMP', table, re.IGNORECASE)]
tables = [table for table in tables if not re.match(b'^T_', table, re.IGNORECASE)]
tables = [table for table in tables if not re.match(b'^Select', table, re.IGNORECASE)]
tables = [table for table in tables if not re.search(b'_shape_index$', table, re.IGNORECASE)]
tables = [table for table in tables if not re.match(b'QuerySelected', table, re.IGNORECASE)]

# Remove unnecessary tables from Biotika (it will be written into for loop or function).
tables = [table for table in tables if not re.match(b'dulo', table, re.IGNORECASE)]
tables = [table for table in tables if not re.match(b'^halo_', table, re.IGNORECASE)]
tables = [table for table in tables if not re.match(b'^kulterulet_', table, re.IGNORECASE)]
tables = [table for table in tables if not re.match(b'^utm', table, re.IGNORECASE)]
tables = [table for table in tables if not re.match(b'^vegetacioterkep', table, re.IGNORECASE)]

# convert mdb tables to csv
for table in tables:
     if table != '':
# Encoding of mdb-tool (based on Faris Madi's article on https://farismadi.wordpress.com/2008/07/13/encoding-of-mdb-tool/)
# The codetable could be spelled like utf-8, UTF8, etc. It depends on OS. Later I'll possible write it with the output of
# iconv -l command.
         subprocess.call("export MDB_ICONV=UTF-8", shell = True)
         subprocess.call("export MDB_{}_CHARSET=UTF-8".format(mdb_ver), shell = True)
         csv = open(table.decode()+".csv", "w")
         csv = str(table.decode()+".csv")
         command = str("mdb-export -D \'%Y-%m-%d %H:%M:%S\' "+DATABASE+ " "+table.decode()+" > "+csv)
         subprocess.call(command, shell=True)

# I had to call the command in shell, because the binary type were written into the csv, and the encoding was wrong, so the methods below
# didn't work. Later I'll rewrite this section as pandas documentaion suggest it in https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html#csv-text-files
# in the "Dealing with Unicode data" section.

# csv = open(table.decode()+".csv", "w")
# sys.stdout = csv
# print (subprocess.Popen(["mdb-export", "-D \'%Y-%m-%d %H:%M:%S\'", DATABASE, table], stdout=subprocess.PIPE).communicate()[0])
# csv.close()

# with open(table.decode()+".csv", 'w') as sys.stdout: print (subprocess.Popen(["mdb-export", DATABASE, "-D \'%Y-%m-%d %H:%M:%S\'", table], stdout=subprocess.PIPE).communicate()[0])

# Get list of csv files in workingdir (if there is another csv in the workingdir, it will be in the list (later I will handle it with the tables list) )
cwd = os.getcwd()

csv_tables = []
with os.scandir(cwd) as dir:
    for entry in dir:
        if not entry.name.startswith('.') and entry.name.endswith(".csv") and entry.is_file():
            csv_tables.append(entry.name)

# We have to drop binary columns from csv, if any. Also want to know the tables with geometry.

geom_tables = []
for csv_table in csv_tables:
    csv_data = pd.read_csv(csv_table, low_memory=False)
    if "SHAPE" in csv_data.columns:
        csv_data = csv_data.drop("SHAPE", axis=1)
        geom_tables.append(csv_table)
    csv_data.to_csv(csv_table)

# In geomdb, as far as I know the Shape column storing geometry as binary. So in this method will only work, if the geometry
# stored in the layer as other format (e.g. WKT geometry or X,Y columns)

# We need to separate the tables with geometry and without geometry

non_geom_tables = list(set(csv_tables)-set(geom_tables))

# csv to spatialite

for table in geom_tables:
    if table is geom_tables[0]:
        first_command = str("ogr2ogr -f \"SQLite\" -s_srs EPSG:23700 -t_srs EPSG:23700 -oo X_POSSIBLE_NAMES=x* -oo Y_POSSIBLE_NAMES=y* -oo Z_POSSIBLE_NAMES=z* -oo KEEP_GEOM_COLUMNS=yes -dsco \'SPATIALITE=yes\' "+NEW_DATABASE+" "+table)
        subprocess.call(first_command, shell=True)
    else:
        command = str("ogr2ogr -f \"SQLite\" -update -s_srs EPSG:23700 -t_srs EPSG:23700 -oo X_POSSIBLE_NAMES=x* -oo Y_POSSIBLE_NAMES=y* -oo Z_POSSIBLE_NAMES=z* -oo KEEP_GEOM_COLUMNS=yes -dsco \'SPATIALITE=yes\' "+NEW_DATABASE+" "+table)
        subprocess.call(command, shell=True)

for table in non_geom_tables:
    command = str("ogr2ogr -f \"SQLite\" -update "+NEW_DATABASE+" "+table)
    subprocess.call(command, shell=True)

# ogr2ogr -f "SQLite" -update -progress -s_srs EPSG:23700 -t_srs EPSG:23700 -oo X_POSSIBLE_NAMES=X -oo Y_POSSIBLE_NAMES=Y -oo KEEP_GEOM_COLUMNS=yes -dsco='SPATILAITE=yes' teszt.sqlite botanikai_adatok.csv

#Clean csv csv_tables

for file in csv_tables:
    os.remove(file)

print("The "+DATABASE+" sucessfully converted. The new spatialite database is: "+NEW_DATABASE)
