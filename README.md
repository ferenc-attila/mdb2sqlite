# MDB to spatialite converter v. 0.1.20200229

Python tool to convert ESRI personal geodatabase (geomdb) files to spatialite database.

As I know, there are three ways of reading geomdb in Linux.

With mdb-tools you can extract tables from access databases. With GDAL/OGR there are two other ways. The old is the PGeo driver (https://gdal.org/drivers/vector/pgeo.html) and there is an alternative from GDAL/OGR 1.9.0 (https://gdal.org/drivers/vector/mdb.html#vector-mdb).

This script using the first method. Later I plan to write the other two ways as this one is very primitive and useless in most of the cases.

The script now supports only point layers with x, y, and z coordinates in the table. Can't read the binary geometry stored in geomdb-s.
It converts mdb tables to csv, removes geomdb specific tables, searches for Shape columns in tables and drops them, as binary geometries useless in csv. After it, as it takes that there are x, y and z columns in these tables (the BNPI Biotika database tables has these columns). Convert these tables to spatialite layers and the other tables also converted to the sqlite database as normal sqlite tables.

Other shortage, that now it is only using the EPSG:23700 crs. It is very simple to change, but it was enough for me to write the crs statically into the code.

Tested on Debian 10 with python 3.7, only with the database mentioned above.

There are two versions of the script. The bnpi_biotika version contains some database specific codes, so it is unusable for any other files. (e.g. in this version you don't have to add filename as an argument, as it is statically written into the code)

Usage in CLI: python3.7 mdb2spatialite.py somefile.mdb
This will create the somefile.sqlite with spatialite extension.

These codes and articles partially used or helped a lot, thank you for the authors:

https://farismadi.wordpress.com/2008/07/13/encoding-of-mdb-tool/
https://www.guyrutenberg.com/2012/07/16/sql-dump-for-ms-access-databases-mdb-files-on-linux/
